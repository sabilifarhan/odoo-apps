{
    'name': "Paid Annual Leave",

    'summary': """
        This module it's for employee to request paid annual leave.""",

    'description': """
        Employees can take annual leave as their right according to applicable regulations. 
        According to Undang-Undang Nomor 13 Tahun 2013, Pasal 79, every employee is entitled to 
        annual leave of at least 12 (twelve) working days.
    """,

    'author': "Farhan Sabili",
    'website': "https://www.linkedin.com/in/billylvn/",

    'category': 'Human Resources/Time Off',
    'license': 'OPL-1',
    'version': '1.0',
    'price': '00.00',
    'currency': 'USD',
    'support': 'sabilifarhan@gmail.com',
    'depends': ['base', 'hr_holidays', 'hr', 'mail', 'hr_contract', 'account'],

    'data': [
        'security/ir.model.access.csv',
        'views/hr_employee.xml',
        'views/paid_leave.xml',
    ],
    'application': True
}
