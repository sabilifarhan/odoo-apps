from odoo import _, api, fields, models
from odoo.exceptions import UserError, RedirectWarning, ValidationError
from _datetime import datetime, date

class PaidLeave(models.Model):
    _name = 'paid.leave'
    _inherit = 'mail.thread'
    _description = 'Paid Annual Leave'

    name = fields.Char(string='Reference', default='/', readonly=True)
    request_date = fields.Date(string='Request Date', default=date.today())
    state = fields.Selection([
        ('draft', 'Draft'),
        ('submit', 'Submitted'),
        ('mgr_approved', 'Manager Approved'),
        ('hr_approved', 'HR Approved'),
        ('refused', 'Refused'),
    ], string='Status', readonly=True, copy=False, default='draft', tracking=True)
    employee_id = fields.Many2one('hr.employee', string='Request by', domain=lambda self: [('id', 'in', self._domain_employee())], default=lambda self: self._default_employee(),required=True, readonly=True, states={'draft': [('readonly', False)]}, tracking=True)
    holiday_status_id = fields.Many2one('hr.leave.type', 'Time Off Type', required=True, readonly=True, states={'draft': [('readonly', False)]}, tracking=True)
    remaining_leave = fields.Float(compute='_compute_leave', string='Remaining Leave (days)')
    number_of_leave = fields.Float(compute='_compute_leave', string='Number of Leave (days)')
    leave_requested = fields.Float(compute='_compute_leave', string='Leave Requested (days)')
    paid_leave = fields.Float(compute='_compute_leave', string='Total Paid Leave (days)')
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id)
    wage = fields.Monetary(string='Wage', related='employee_id.contract_id.wage', copy=False)
    resignation_date = fields.Date(string='Resignation Date', related="employee_id.date_of_resignation")
    working_day = fields.Integer(string='Actual Working Day', help='Actual working day on month of resignation date employee.', default=22, readonly=True, states={'draft': [('readonly', False)]}, tracking=True)
    contract_id = fields.Many2one('hr.contract', string='Contract', related='employee_id.contract_id')
    total = fields.Monetary(string='Total', copy=False)
    reason_refuse = fields.Text(string='Reason for Refusal', copy=False, tracking=True)
    refused_user_id = fields.Many2one('res.users', string='Refused by', tracking=True)
    approval_id = fields.Many2one('res.users', string='Time Off Approver', related='employee_id.leave_manager_id')
    show_button_approve = fields.Boolean(compute='_compute_show_button_approve', string='Show Button Approve')
    bills_count = fields.Integer(compute='_compute_bills_count', string='Total Bills')
    move_line = fields.One2many('account.move', 'paid_leave_id', string='Bills')
    fully_paid = fields.Boolean(compute='_compute_fully_paid', string='Fully Paid')
    
    def unlink(self):
        res = super(PaidLeave, self).unlink()
        for i in self:
            if i.move_line:
                raise UserError("%s already have any bills, delete it first." % (i.name))
            else:
                return res

    @api.depends('move_line')
    def _compute_fully_paid(self):
        for i in self:
            i.fully_paid = i.move_line.payment_state == 'paid'

    @api.depends('move_line')
    def _compute_bills_count(self):
        for i in self:
            i.bills_count = len(i.move_line)

    def action_show_bills(self):
        self.ensure_one()
        return {
            'name': _('Bills'),
            'view_mode': 'form',
            'res_model': 'account.move',
            'type': 'ir.actions.act_window',
            'context': {'create': False, 'delete': False},
            'res_id': self.move_line.id,
            'target': 'current',
        }

    def create_bill(self):
        self = self.sudo()
        for i in self:
            i.move_line = [(0,0, {
                'ref': i.name,
                'invoice_date': date.today(),
                'move_type': 'in_invoice',
                'invoice_line_ids': [(0,0, {
                    'name': 'Paid Annual Leave - %s' % (i.employee_id.name),
                    'price_unit': i.total
                })]
            })]

    def _compute_show_button_approve(self):
        for i in self:
            i.show_button_approve = ((i.state == 'mgr_approved' or (i.state == 'submit' and not i.approval_id)) and i.user_has_groups("hr.group_hr_manager")) or (i.state == 'submit' and i.approval_id.id == i.env.user.id)

    def _compute_leave(self):
        for i in self:
            if i.resignation_date:
                query = """ with master_leave as (select sum(hlr.number_of_days) as number_leave,he.id as employee
                from hr_leave_report hlr
                left join hr_employee he on he.id = hlr.employee_id
                where hlr.number_of_days > 0 and hlr.employee_id is not null and
                hlr.holiday_status_id = %s
                group by employee),

                leave_requested as(
                SELECT he.id as employee, coalesce(sum(hl.number_of_days),0) as requested
                FROM hr_leave hl
                LEFT JOIN hr_employee he on he.id = hl.employee_id
                WHERE hl.holiday_status_id = %s and hl.state = 'validate' AND hl.request_date_to <= '%s'
                group by employee
                )

                SELECT COALESCE(ml.number_leave, 0) number_leave,coalesce(lr.requested,0) as requested
                FROM master_leave as ml
                LEFT JOIN leave_requested lr on lr.employee = ml.employee
                WHERE ml.employee = %s
                LIMIT 1
                    """ % (i.holiday_status_id.id, i.holiday_status_id.id, i.resignation_date,i.employee_id.id)
                self.env.cr.execute(query)
                data = self.env.cr.dictfetchone()
                if data:
                    i.number_of_leave = int(data['number_leave'])
                    i.remaining_leave = int(i.resignation_date.strftime('%m')) / 12 * int(data['number_leave'])
                    i.leave_requested = int(data['requested'])
                    i.paid_leave = i.remaining_leave - int(data['requested'])
                else:
                    i.number_of_leave = 0
                    i.remaining_leave = 0
                    i.leave_requested = 0
                    i.paid_leave = 0
            else:
                i.number_of_leave = 0
                i.remaining_leave = 0
                i.leave_requested = 0
                i.paid_leave = 0

    def compute_amount(self):
        if not self.wage:
            raise ValidationError('This employee does not have any running contract.')
        if not self.resignation_date:
            raise ValidationError('This employee is not have resignation date.')
        self.total = (self.paid_leave / self.working_day) * self.wage

    def set_to_draft(self):
        for i in self:
            i.write({'state': 'draft'})

    def refuse(self):
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'leave.refused',
            'view_id': self.env.ref("fs14_paid_annual_leave.leave_refusal_form").id,
            'target': 'new',
        }

    def approve(self):
        for i in self:
            if (not i.approval_id and i.state == 'submit') or (i.state == 'mgr_approved'):
                return i.write({'state': 'hr_approved'})
            i.write({'state': 'mgr_approved'})

    def submit(self):
        for i in self:
            i.write({'state': 'submit'})
        
    def _domain_employee(self):
        return self.env.user.employee_ids.ids
    
    def _default_employee(self):
        return self.env.user.employee_ids[0].id

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        if not self.employee_id.date_of_resignation:
            return {
                'warning':{
                    'title': 'Warning.',
                    'message': '%s is not have resignation date.' % (self.employee_id.name)
                }
            }

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('paid.leave')
        return super(PaidLeave, self).create(vals)

class LeaveRefused(models.TransientModel):
    _name = 'leave.refused'
    _description = 'Leave Refused'

    user_id = fields.Many2one('res.users', string='Refused by', default=lambda self: self.env.user.id)
    reason_refuse = fields.Text(string="Reason for Refusal")

    def refuse(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            leave = self.env["paid.leave"].browse(active_id)
            leave.state = 'refused'
            leave.reason_refuse = self.reason_refuse
            leave.refused_user_id = self.user_id
    