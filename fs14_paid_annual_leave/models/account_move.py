from odoo import _, api, fields, models

class AccountMove(models.Model):
    _inherit = 'account.move'

    paid_leave_id = fields.Many2one('paid.leave', string='Paid Annual Leave')