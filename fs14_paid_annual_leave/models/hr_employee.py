from odoo import models, fields, api

class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    date_of_resignation = fields.Date(string='Date of Resignation', help='Date of resignation for employee.')    