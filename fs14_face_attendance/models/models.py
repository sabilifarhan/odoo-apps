from odoo import _, api, fields, models

import re
import json
from urllib.request import urlopen
import base64
from io import BytesIO as StringIO


class HrAttendance(models.Model):
    _inherit = 'hr.attendance'

    location_check_in = fields.Char('Location Check In')
    location_check_out = fields.Char('Location Check Out')

    pict_check_in = fields.Binary('Picture Check In')
    pict_check_out = fields.Binary('Picture Check Out')

    def create(self, vals):
        res = super(HrAttendance, self).create(vals)
        url = 'http://ipinfo.io/json'
        response = urlopen(url)
        data = json.load(response)
        res.location_check_in = data['loc']
        return res

    def write(self, vals):
        res = super(HrAttendance, self).write(vals)
        if 'check_out' in vals:
            url = 'http://ipinfo.io/json'
            response = urlopen(url)
            data = json.load(response)
            self.location_check_out = data['loc']
        return res

class HrEmployeeBase(models.AbstractModel):
    _inherit = 'hr.employee.base'


    def attendance_manual(self, next_action, code, entered_pin=None):
        self.ensure_one()
        length = len(code)
        final_code = code[22:length]
        can_check_without_pin = not self.env.user.has_group('hr_attendance.group_hr_attendance_use_pin') or (self.user_id == self.env.user and entered_pin is None)
        if can_check_without_pin or entered_pin is not None and entered_pin == self.sudo().pin:
            return self._attendance_action(next_action, final_code)
        return {'warning': _('Wrong PIN')}

    def _attendance_action(self, next_action, code):
        """ Changes the attendance of the employee.
            Returns an action to the check in/out message,
            next_action defines which menu the check in/out message should return to. ("My Attendances" or "Kiosk Mode")
        """
        self.ensure_one()
        employee = self.sudo()
        action_message = self.env.ref('hr_attendance.hr_attendance_action_greeting_message').read()[0]
        action_message['previous_attendance_change_date'] = employee.last_attendance_id and (employee.last_attendance_id.check_out or employee.last_attendance_id.check_in) or False
        action_message['employee_name'] = employee.name
        action_message['barcode'] = employee.barcode
        action_message['next_action'] = next_action
        action_message['hours_today'] = employee.hours_today

        if employee.user_id:
            modified_attendance = employee.with_user(employee.user_id)._attendance_action_change(code)
        else:
            modified_attendance = employee._attendance_action_change(code)
        action_message['attendance'] = modified_attendance.read()[0]
        return {'action': action_message}

    def _attendance_action_change(self, code):
        url = 'http://ipinfo.io/json'
        response = urlopen(url)
        data = json.load(response)
        crd = data['loc'].split(",")
        """ Check In/Check Out action
            Check In: create a new attendance record
            Check Out: modify check_out field of appropriate attendance record
        """
        self.ensure_one()
        action_date = fields.Datetime.now()

        if self.attendance_state != 'checked_in':
            vals = {
                'employee_id': self.id,
                'check_in': action_date,
                'pict_check_in': code,
            }
            return self.env['hr.attendance'].create(vals)
        attendance = self.env['hr.attendance'].search([('employee_id', '=', self.id), ('check_out', '=', False)], limit=1)
        if attendance:
            attendance.check_out = action_date
            attendance.pict_check_out = code
        else:
            raise exceptions.UserError(_('Cannot perform check out on %(empl_name)s, could not find corresponding check in. '
                'Your attendances have probably been modified manually by human resources.') % {'empl_name': self.sudo().name, })
        return attendance