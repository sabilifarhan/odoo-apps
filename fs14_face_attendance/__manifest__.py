{
    'name': "Face Attendance",

    'summary': """
        Employee attendance with capturing face and coordinates.""",

    'description': """
        Employee attendance with capturing face and coordinates.
    """,

    'author': "Farhan Sabili",
    'website': "https://www.linkedin.com/in/billylvn/",

    'category': 'Human Resources',
    'version': '14.0.0',

    'depends': ['base', 'hr', 'hr_attendance'],

    'data': [
        'views/templates.xml',
        'views/views.xml',
    ],
    'application': True,
    'qweb': [
        'static/src/xml/attendance.xml',
    ]
}
