# -*- coding: utf-8 -*-
{
    'name': "PDF Password Protection",

    'summary': """
        Secure your PDF file/report with configurable password.""",

    'description': """
        Secure your PDF file/report with configurable password.
    """,

    'author': "Farhan Sabili",
    'website': "https://www.linkedin.com/in/billylvn/",

    'category': 'Tools',
    'version': '0.1',

    'depends': ['base'],

    'data': [
        'security/security.xml',
        'views/views.xml',
    ],
}
