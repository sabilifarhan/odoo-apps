from odoo import _, api, fields, models
import io
from odoo.tools import pdf

class IrActionsReport(models.Model):
    _inherit = 'ir.actions.report'

    encrypt_pdf = fields.Boolean('Encrypt')
    password_type = fields.Selection([
        ('static', 'Static'),
        ('dynamic', 'Dynamic (Depends on any fields)'),
    ], string='Password Type', default='static')
    password = fields.Char('Password')
    password_field_id = fields.Many2one('ir.model.fields', string='Field Name')
    multiple_record = fields.Char('Multiple Record Password')

    def encrypt(self, raw_pdf, password):
        out_pdf = io.BytesIO(raw_pdf)
        reader = pdf.OdooPdfFileReader(out_pdf)

        pdf_writer = pdf.OdooPdfFileWriter()
        pdf_writer.cloneReaderDocumentRoot(reader)
        pdf_writer.encrypt(password)

        with io.BytesIO() as writer_buffer:
            pdf_writer.write(writer_buffer)
            encrypted_content = writer_buffer.getvalue()

        return encrypted_content
        
    def _render_qweb_pdf(self, res_ids=None, data=None):
        
        def _getattrstring(obj, field_str):
            field_value = obj[field_str]
            return str(field_value)

        res,x = super(IrActionsReport, self)._render_qweb_pdf(res_ids=res_ids, data=data)
        if self.encrypt_pdf and self.report_type == 'qweb-pdf':
            if len(res_ids) > 1:
                password = self.multiple_record
            else:
                password = self.password
                if self.password_type == 'dynamic':
                    recs = self.env[self.model].browse(res_ids)
                    password = _getattrstring(recs, self.password_field_id.name)
            encrypted = self.encrypt(res, password)
            return encrypted,x
        return res,x

    