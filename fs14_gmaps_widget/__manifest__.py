{
    'name': "Google Maps Widget",

    'summary': """
        Embed google maps on any field, depends on coordinate or location name.""",

    'description': """
        How to use:
        1. Add widget="embed_gmaps" on field char/text.
        2. Enjoy.
    """,

    'author': "Farhan Sabili",
    'website': "https://www.linkedin.com/in/billylvn/",

    'category': 'Productivity',
    'version': '14.0.0',

    'depends': ['base'],

    'data': [
        'views/templates.xml',
    ],
    'application': True,
    'qweb': [
        'static/src/xml/gmaps_widget.xml',
    ]
}
