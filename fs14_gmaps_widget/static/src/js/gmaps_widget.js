odoo.define("fs14_gmaps_widget.GmapsWidget", function(require){
    "use strict";

    var AbstractField = require('web.AbstractField');
    var FieldRegistry = require('web.field_registry');
    var core = require('web.core');
    var QWeb = core.qweb;
    var GmapsWidget = AbstractField.extend({
        
        supportedFieldTypes: ['char', 'text'],
        template: 'GmapsWidgetTemplate', 
        events: {
            'change .gmaps_widget': 'updateValue',
        },
        updateValue: function () {
            var coordinat = this.$('.gmaps_widget')[0].value
            this._setValue(coordinat);
        },
        setSrcGmaps: function (value) {
            var coordinate = value.replace(" ", "%20");
            let raw_links = "https://maps.google.com/maps?q=GMAPS_WIDGET&t=&z=13&ie=UTF8&iwloc=&output=embed";
            let links = raw_links.replace("GMAPS_WIDGET", coordinate);
            return links
        },
        _render: function () {
            var links = this.setSrcGmaps(this.value)
            this.$el.html($(QWeb.render(this.template, {'widget': this, 'coordinate': this.value, 'links': links})));
        },
        
    });
    
    
    FieldRegistry.add('embed_gmaps', GmapsWidget);
 
    return GmapsWidget;
});