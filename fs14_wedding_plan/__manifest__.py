# -*- coding: utf-8 -*-
{
    'name': "Wedding Plan",

    'summary': """
        Wedding Plan Management.""",

    'description': """
        Wedding Plan Management.
    """,

    'author': "Farhan Sabili",
    'website': "https://www.linkedin.com/in/billylvn/",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base', 'mail', 'website'],

    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'wizards/views.xml',
        'views/assets.xml',
        'views/templates.xml',
        'views/wedding_calendar.xml',
        'views/wedding_budget.xml',
        'views/wedding_invitation.xml',
        'views/wedding_expenses.xml',
        'views/wedding_session.xml',
        'views/wedding_wishes.xml',
        'views/menu_items.xml',
    ],
    'application': True
}
