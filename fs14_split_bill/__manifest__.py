{
    'name': "Split Bill",

    'summary': """
        Split your bill for easy paid share.""",

    'description': """
        Split your bill for easy paid share.
    """,

    'author': "Farhan Sabili",
    'website': "https://www.linkedin.com/in/billylvn/",

    'category': 'Productivity',
    'version': '1.1',

    'depends': ['base','mail'],

    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
    ],
    'application': True
}
