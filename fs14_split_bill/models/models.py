from odoo import _, api, fields, models
from odoo.exceptions import ValidationError
class SplitBill(models.Model):
    _name = 'split.bill'
    _description = 'Split Bill'
    _inherit = ["mail.thread", "mail.activity.mixin"]
    
    name = fields.Char(tracking=True)
    partner_id = fields.Many2one('res.partner', string='Responsible', tracking=True)
    total_discount = fields.Monetary('Total Discounts', tracking=True)
    total_cost = fields.Monetary('Additional Costs', tracking=True)
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('paid', 'Paid'),
        ('reject', 'Rejected'),
    ], string='Status', copy=False, default='draft', tracking=True)
    note = fields.Text(tracking=True)
    bill_line_ids = fields.One2many('split.bill.line', 'bill_id', string='Bill Line')
    grand_total = fields.Monetary(compute='_compute_grand_total', string='Grand Total')

    def unlink(self):
        for rec in self:
            if rec.state != 'draft':
                raise ValidationError(("You can't delete this document when state is not draft."))
            return super(SplitBill, rec).unlink()
    
    def _compute_grand_total(self):
        for rec in self:
            rec.grand_total = sum(rec.bill_line_ids.mapped('subtotal'))
    
    def compute(self):
        for rec in self:
            total_nett = sum(rec.bill_line_ids.mapped('nett'))
            for line in rec.bill_line_ids:
                line.percent_discount = line.nett/total_nett
                line.amount_discount = line.percent_discount*rec.total_discount

    def confirm(self):
        for rec in self:
            rec.write({'state': 'paid'})

    def reject(self):
        for rec in self:
            rec.write({'state': 'reject'})

    def set_to_draft(self):
        for rec in self:
            rec.write({'state': 'draft'})

class SplitBillLine(models.Model):
    _name = 'split.bill.line'
    _description = 'Split Bill Line'
    
    bill_id = fields.Many2one('split.bill', string='Split Bill')
    partner_id = fields.Many2one('res.partner', string='Name')
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id)
    purchase_price = fields.Monetary('Purchase Price')
    total_cost = fields.Monetary('Additional Costs', compute="get_total_cost")
    nett = fields.Monetary(compute='compute_total', string='Nett')
    percent_discount = fields.Float('Discount Percent')
    amount_discount = fields.Monetary('Discount Amount')
    subtotal = fields.Monetary('Subtotal', compute='compute_total')

    def get_total_cost(self):
        for rec in self:
            rec.total_cost = rec.bill_id.total_cost / len(rec.bill_id.bill_line_ids)

    @api.depends('purchase_price', 'total_cost')
    def compute_total(self):
        for rec in self:
            rec.nett = rec.purchase_price + rec.total_cost
            rec.subtotal = rec.nett - rec.amount_discount

