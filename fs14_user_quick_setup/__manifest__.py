{
    'name': "User Quick Setup",

    'summary': """
        Quick Setup User.""",

    'description': """
        Quick Setup User to create employee and contract with easy step.
    """,

    'author': "Farhan Sabili",
    'website': "https://www.linkedin.com/in/billylvn/",
    'category': 'Productivity',
    'version': '0.1',

    'depends': ['base', 'hr', 'hr_contract', 'hr_holidays'],

    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
    ],
    'application': True
}
