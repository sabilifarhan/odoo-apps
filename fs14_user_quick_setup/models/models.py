from odoo import _, api, fields, models
from odoo.addons.base.models.res_partner import _tz_get
from odoo.exceptions import ValidationError, AccessDenied
class QuickSetupUsers(models.Model):
    _name = 'quick.setup.users'
    _description = 'Quick Setup Users'
    _sql_constraints = [
        ('restriction_name', 'unique (name)',
         'This name is already registered.'),
         ('restriction_email', 'unique (email)',
         'This email is already registered.')
    ]

    name = fields.Char(string='Name', copy=False)
    company_id = fields.Many2one('res.company', string='Company')
    email = fields.Char(string='Email', copy=False)
    password = fields.Char(string='Password', copy=False)
    profile_picture = fields.Binary(string='Profile Picture')
    group_ids = fields.Many2many('res.groups', string='Groups')
    tz = fields.Selection(_tz_get, string="Timezone")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('submit', 'Submitted'),
        ('done', 'Done')
    ], string='Status', default='draft', copy=False)
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id)
    user_ids = fields.One2many('res.users', 'quick_setup_id', string='User')

    # Employee
    employee_ids = fields.One2many('hr.employee', 'quick_setup_id', string='Employee')
    create_employee = fields.Boolean(string='Create Employee')
    resource_calendar_id = fields.Many2one('resource.calendar', string='Working Hours')
    department_id = fields.Many2one('hr.department', string='Department')
    job_id = fields.Many2one('hr.job', string='Job Position')
    mobile_phone = fields.Char(string='Work Mobile')
    work_email = fields.Char(string='Work Email')
    work_phone = fields.Char(string='Work Phone')

    # Contract
    contract_ids = fields.One2many('hr.contract', 'quick_setup_id', string='Contract')
    create_contract = fields.Boolean(string='Create Contract')
    contract_ref = fields.Char(string='Contract Reference')
    date_start = fields.Date(string='Start Date')
    date_end = fields.Date(string='End Date')
    wage = fields.Monetary(string='Wage')
    structure_type_id = fields.Many2one('hr.payroll.structure.type', string='Salary Structure Type')

    # Leave Allocation
    allocation_ids = fields.One2many('hr.leave.allocation', 'quick_setup_id', string='Allocation')
    create_allocation = fields.Boolean('Create Allocation')
    allocation_ref = fields.Char('Allocation Name')
    holiday_status_id = fields.Many2one('hr.leave.type', string='Time Off Type')
    duration = fields.Float('Duration')

    def action_show_user(self):
        self.ensure_one()
        return {
            'name': _('User'),
            'view_mode': 'form',
            'res_model': 'res.users',
            'type': 'ir.actions.act_window',
            'context': {'create': False},
            'res_id': self.user_ids.id,
            'target': 'current',
        }

    def action_show_employee(self):
        self.ensure_one()
        return {
            'name': _('Employee'),
            'view_mode': 'form',
            'res_model': 'hr.employee',
            'type': 'ir.actions.act_window',
            'context': {'create': False},
            'res_id': self.employee_ids.id,
            'target': 'current',
        }

    def action_show_contract(self):
        self.ensure_one()
        return {
            'name': _('Contract'),
            'view_mode': 'form',
            'res_model': 'hr.contract',
            'type': 'ir.actions.act_window',
            'context': {'create': False},
            'res_id': self.contract_ids.id,
            'target': 'current',
        }
    
    def action_show_allocation(self):
        self.ensure_one()
        return {
            'name': _('Time Off Allocation'),
            'view_mode': 'form',
            'res_model': 'hr.leave.allocation',
            'type': 'ir.actions.act_window',
            'context': {'create': False},
            'res_id': self.allocation_ids.id,
            'target': 'current',
        }

    @api.onchange('create_employee')
    def _onchange_create_employee(self):
        if not self.create_employee:
            self.create_contract = False
            self.create_allocation = False

    def generate(self):
        groups = [self.env.ref('base.group_user').id]
        if self.group_ids:
            for group in self.group_ids.ids:
                groups.append(group)
        self.user_ids = [(0,0,{
            'name': self.name,
            'email': self.email,
            'login': self.email,
            'groups_id': [(6,0, groups)],
            'image_1920': self.profile_picture,
            'password': self.password
        })]
        if self.create_employee:
            self.employee_ids = [(0,0, {
                'name': self.name,
                'resource_calendar_id': self.resource_calendar_id.id,
                'department_id': self.department_id.id,
                'job_id': self.job_id.id,
                'mobile_phone': self.mobile_phone,
                'work_email': self.work_email,
                'work_phone': self.work_phone,
                'user_id': self.user_ids.id
            })]
            if self.create_contract:
                self.contract_ids = [(0,0, {
                    'name': self.contract_ref,
                    'employee_id': self.employee_ids.id,
                    'department_id': self.employee_ids.department_id.id,
                    'job_id': self.employee_ids.job_id.id,
                    'date_start': self.date_start,
                    'date_end': self.date_end,
                    'structure_type_id': self.structure_type_id.id,
                    'wage': self.wage,
                    'resource_calendar_id': self.resource_calendar_id.id,
                    'state': 'open'
                })]
            if self.create_allocation:
                self.allocation_ids = [(0,0, {
                    'name': self.allocation_ref,
                    'holiday_status_id': self.holiday_status_id.id,
                    'allocation_type': 'regular',
                    'number_of_days': self.duration,
                    'holiday_type': 'employee',
                    'employee_id': self.employee_ids.id
                })]
                self.allocation_ids.action_approve()

    def submit(self):
        for rec in self:
            rec.state = 'submit'

    def draft(self):
        for rec in self:
            rec.state = 'draft'

    def done(self):
        for rec in self:
            if not rec.user_has_groups("fs14_user_quick_setup.group_quick_setup_admin"):
                raise AccessDenied(("Only administrator can generate this document."))
            if rec.state != 'done':
                rec.state = 'done'
                rec.generate()
    
    def unlink(self):
        for rec in self:
            if rec.employee_ids or rec.contract_ids or rec.user_ids or rec.allocation_ids:
                raise ValidationError(("You can't delete generated document, please delete related document first."))
            else:
                super(QuickSetupUsers, rec).unlink()

class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    quick_setup_id = fields.Many2one('quick.setup.users', string='Quick Setup')

class HrContract(models.Model):
    _inherit = 'hr.contract'
    
    quick_setup_id = fields.Many2one('quick.setup.users', string='Quick Setup')

class HrLeaveAllocation(models.Model):
    _inherit = 'hr.leave.allocation'
    
    quick_setup_id = fields.Many2one('quick.setup.users', string='Quick Setup')

class ResUsers(models.Model):
    _inherit = 'res.users'
    
    quick_setup_id = fields.Many2one('quick.setup.users', string='Quick Setup')
