{
    'name': "Dynamic Approval Purchase Order",

    'summary': """
        Created for company with dynamic approval on Purchase Order.""",

    'description': """
        This module can setting approval for Purchase Order depending by Range of maximum order.
    """,

    'author': "Farhan Sabili",
    'website': "https://www.linkedin.com/in/billylvn/",

    'category': 'Purchases',
    'support': 'https://www.linkedin.com/in/billylvn/',
    'images': ['static/description/companies.png'],
    'depends': ['base', 'purchase'],

    'data': [
        'security/ir.model.access.csv',
        'views/company.xml',
        'views/purchase.xml',
        'wizard/wizard.xml',
    ],
    'application': True
}
