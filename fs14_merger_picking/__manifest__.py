{
    'name': "Merger Picking",

    'summary': """
        This module allow you to merger document picking like delivery order, incoming shipment,and internal transfer.""",

    'description': """
        You can easily merger document picking on list view with an action "Merge Picking"
    """,

    'author': "Farhan Sabili",
    'website': "https://www.linkedin.com/in/billylvn/",
    'category': 'Inventory',
    'version': '0.1',

    'depends': ['base','stock'],

    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
    ],
}
