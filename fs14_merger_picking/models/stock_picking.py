from odoo import _, api, fields, models
from odoo.exceptions import UserError

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    def raw_data_merge(self):
        if len(self) < 2:
            raise UserError(('Merge at least 2 pickings or more.'))

        for i in self:
            if i.state in ['done','cancel']:
                raise UserError(('You are not allowed to merge with Done or Cancel pickings.'))
        
        state = self.mapped('state')
        partner = self.mapped('partner_id.id')
        type = self.mapped('picking_type_id.id')
        source = self.mapped('location_id.id')
        dest = self.mapped('location_dest_id.id')
        origin = self.mapped('name')
        if not state.count(state[0]) == len(state):
            raise UserError(('You are not allowed to merge with different document status.'))
        if not partner.count(partner[0]) == len(partner):
            raise UserError(('You are not allowed to merge with different partner.'))
        if not type.count(type[0]) == len(type):
            raise UserError(('You are not allowed to merge with different document type.'))
        if not source.count(source[0]) == len(source):
            raise UserError(('You are not allowed to merge with different source location.'))
        if not dest.count(dest[0]) == len(dest):
            raise UserError(('You are not allowed to merge with different destination location.'))
        
        data = {'partner_id': partner[0], 'picking_type_id': type[0], 'location_id': source[0], 'location_dest_id': dest[0], 'origin': origin}
        return data

    def prepare_merged_header(self, partner_id, picking_type_id, location_id, location_dest_id, origin):
        data = {
            'partner_id': partner_id,
            'picking_type_id': picking_type_id,
            'location_id': location_id,
            'location_dest_id': location_dest_id,
            'origin': ','.join(map(str,origin)),
        }
        return data

    def prepare_merged_line(self):
        query = """ SELECT product_id, product_uom, location_id, location_dest_id, sum(product_uom_qty) as product_uom_qty 
        FROM stock_move
        WHERE picking_id in %s
        GROUP by product_id, product_uom, location_id, location_dest_id
        ORDER by product_id asc
         """ % (str(tuple(self.ids)))
        self._cr.execute(query)
        result = self._cr.dictfetchall()
        return result

    def merge_picking(self):
        obj_picking = self.env['stock.picking']
        data = self.raw_data_merge()
        header = self.prepare_merged_header(data['partner_id'],data['picking_type_id'],data['location_id'],
        data['location_dest_id'],data['origin'])
        merged = obj_picking.create(header)
        raw_line = self.prepare_merged_line()
        for line in raw_line:
            line['name'] = data['origin']
            merged.update({'move_lines': [(0,0,line)]})
        merged.action_confirm()
        for i in self:
            i.write({'state': 'cancel'})
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'stock.picking',
            'res_id': merged.id,
        }