# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools


class BudgetAnalysis(models.Model):
    _name = 'budget.analysis'
    _auto = False
    _description = 'Budget Analysis'

    date = fields.Date()
    category_id = fields.Many2one('budget.category')
    realization_id = fields.Many2one('budget.realization', string='Realization')
    realization_line_id = fields.Many2one('budget.realization.line', string="Realization Detail")
    total_amount = fields.Float()

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self._cr.execute("""CREATE OR REPLACE VIEW %s AS (
            SELECT
                max(brl.id) AS id,
                brl.id as realization_line_id,
                brl.realization_id as realization_id,
                coalesce(sum(brl.amount), 0) as total_amount,
                bpl.category_id as category_id,
                br.date as date
            FROM 
                budget_realization_line brl
                LEFT JOIN budget_realization br on br.id = brl.realization_id
                LEFT JOIN budget_planning_line bpl on bpl.id = brl.budget_line_id
                LEFT JOIN budget_category bc on bc.id = bpl.category_id

            WHERE br.state = 'done'

            GROUP BY br.date, brl.id, bpl.category_id, brl.realization_id
            ORDER BY br.date desc
        )
        """ % self._table)
