from odoo import _, api, fields, models
from odoo.exceptions import ValidationError

class BudgetPlanning(models.Model):
    _name = 'budget.planning'
    _description = 'Budget Planning'
    _order = 'name desc'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    
    name = fields.Char('')
    description = fields.Char(tracking=True)
    active = fields.Boolean(default=True)
    effective_date = fields.Date('Effective Date', tracking=True)
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id, tracking=True)
    beginning_balance = fields.Monetary('Beginning Balance', tracking=True, compute="_get_balance")
    ending_balance = fields.Monetary('Ending Balance', compute="_get_balance", tracking=True)
    consumed = fields.Float(compute='_get_balance', string='Consumed', tracking=True)
    line_ids = fields.One2many('budget.planning.line', 'budget_id', string='Line', tracking=True, copy=True)
    realization_line_ids = fields.One2many('budget.realization.line', 'planning_id', string='Realization', domain=[('realization_id.state','=','done')])
    realization_ids = fields.One2many('budget.realization', 'planning_id', string='Realization')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('run', 'Running'),
        ('done', 'Closed'),
    ], string='Status', copy=False, default='draft', tracking=True)
    count_realization = fields.Integer(compute='_compute_count_realization', string='Count Realization')
    

    @api.depends('realization_ids')
    def _compute_count_realization(self):
        for rec in self:
            rec.count_realization = len(rec.realization_ids)

    def show_realization(self):
        return {
            'name': 'Budget Realization',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,kanban,form' if self.count_realization >= 1 else 'form',
            'target': 'current',
            'domain': [('id','in',self.realization_ids.ids)],
            'res_model': 'budget.realization',
            'context': {'default_planning_id': self.id, 'create': self.state == 'run'}
        }

    def name_get(self):
        return [(i.id, "%s (%s)" % (i.name, i.description)) for i in self]

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('budget.planning')
        return super(BudgetPlanning, self).create(vals)

    def button_draft(self):
        self.state = 'draft'

    def button_run(self):
        self.state = 'run'

    def button_lock(self):
        self.state = 'done'
        clearance_lines = []
        for line in self.line_ids.filtered(lambda x: x.remaining > 0):
            clearance_lines += [(0,0, {
                'name': 'Clearance',
                'budget_line_id': line.id,
                'amount': line.remaining
            })]
        if clearance_lines:
            clearance_realz = self.env['budget.realization'].create({
                'description': 'Clearance ' + self.description,
                'planning_id': self.id,
                'line_ids': clearance_lines
            })
            clearance_realz.button_done()

    @api.depends('line_ids','realization_line_ids')
    def _get_balance(self):
        for rec in self:
            rec.beginning_balance = sum(rec.line_ids.mapped('amount'))
            rec.ending_balance = rec.beginning_balance - sum(rec.realization_line_ids.mapped('amount'))
            try:
                rec.consumed = (sum(rec.realization_line_ids.mapped('amount')) / rec.beginning_balance) * 100
            except:
                rec.consumed = 0

class BudgetPlanningLine(models.Model):
    _name = 'budget.planning.line'
    _description = 'Budget Planning Line'
    _order = 'sequence asc'

    name = fields.Char('Description')
    sequence = fields.Integer('Sequence')
    category_id = fields.Many2one('budget.category', string='Category')
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id)
    amount = fields.Monetary('')
    budget_id = fields.Many2one('budget.planning', string='Budget')
    realization_line_ids = fields.One2many('budget.realization.line', 'budget_line_id', string='Realization Line', domain=[('realization_id.state','=','done')])
    realization = fields.Monetary(compute='_compute_realization', string='Realization')
    remaining = fields.Monetary('Remaining Amount', compute="_compute_realization")
    
    def show_realization(self):
        return {
            'name': 'Budget Realization - %s' % self.name,
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,kanban,form',
            'target': 'current',
            'domain': [('id','in',self.realization_line_ids.ids)],
            'res_model': 'budget.realization.line',
            'context': {'create': False, 'edit': False, 'delete': False}
        }
    
    @api.depends('realization_line_ids')
    def _compute_realization(self):
        for rec in self:
            rec.realization = sum(rec.realization_line_ids.mapped('amount'))
            rec.remaining = rec.amount - rec.realization

    def name_get(self):
        return [(i.id, "%s %s" % (i.name, "("+i.category_id.name+")" if i.category_id else "")) for i in self]