# -*- coding: utf-8 -*-
{
    'name': "Working Calendar - Indonesia",

    'summary': """
        Generate public holiday calendar Indonesia on working calendar.""",

    'description': """
        Generate public holiday calendar Indonesia on working calendar.
    """,

    'author': "Farhan Sabili",
    'website': "https://www.linkedin.com/in/billylvn/",

    'category': 'Human Resources',
    'version': '14.0.0',

    'depends': ['base','hr_holidays'],

    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/templates.xml',
        'views/working_calendar.xml',
    ],
    'application': True
}
