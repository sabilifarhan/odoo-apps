from odoo import _, api, fields, models
from _datetime import datetime, date
from odoo.exceptions import AccessDenied, UserError
import requests

class WorkingCalendar(models.Model):
    _name = 'working.calendar'
    _description = 'Working Calendar'

    name = fields.Char('Name')
    type = fields.Selection([
        ('company', 'By Company'),
        ('department', 'By Department'),
        ('employee', 'By Employee'),
    ], string='Mode', default='company')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    department_id = fields.Many2one('hr.department', string='Department')
    company_id = fields.Many2one('res.company', string='Company')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('submit', 'Submitted'),
        ('approve', 'Approved'),
        ('done', 'Done'),
    ], string='Status', copy=False, default='draft')
    year = fields.Selection([(str(num), str(num)) for num in range(2020, (datetime.now().year)+3 )], 'Year', default=str(datetime.now().year), copy=False)
    line_ids = fields.One2many('working.calendar.line', 'working_calendar_id', string='Working Calendar Line')

    def get_working_days(self, working_hour):
        res = []
        raw = working_hour.attendance_ids
        for r in raw:
            day = dict(r._fields['dayofweek'].selection).get(r.dayofweek)
            if day not in res:
                res.append(day)
        return res

    def create_time_off(self, type):
        obj_leave = self.env['hr.leave']
        works = self.get_working_days(self.employee_id.resource_calendar_id)
        res = {
            'holiday_type': type,
            'number_of_days': 1,
        }
        if type == 'company':
            res['mode_company_id'] = self.company_id.id
        elif type == 'department':
            res['department_id'] = self.department_id.id
        elif type == 'employee':
            res['employee_id'] = self.employee_id.id
            
        for line in self.line_ids:
            if line.day in works:
                holiday_type = self.env.ref('fs14_working_calendar.public_holiday_type').id if line.type == 'holiday' else self.env.ref('fs14_working_calendar.internal_event_type').id
                res['holiday_status_id'] = holiday_type
                res['request_date_from'] = line.date
                res['request_date_to'] = line.date
                res['date_from'] = line.date
                res['date_to'] = line.date
                res['name'] = line.description
                leaves = obj_leave.create(res)


    def submit(self):
        for i in self:
            i.write({'state': 'submit'})

    def approve(self):
        for i in self:
            i.write({'state': 'approve'})

    def generate(self):
        for i in self:
            i.create_time_off(i.type)
            i.write({'state': 'done'})

    def draft(self):
        for i in self:
            i.write({'state': 'draft'})

    def unlink(self):
        for rec in self:
            if rec.state != 'draft':
                raise UserError(("You can't delete when state is not draft."))
            else:
                return super(WorkingCalendar, rec).unlink()

    def get_working_calendar(self):
        try:
            self.line_ids = [(5,0,0)]
            req = requests.get('https://api-harilibur.vercel.app/api?year=%s' % (self.year), headers={'Accept':'application/json'})
            raw = req.json()
            for r in raw:
                if r['is_national_holiday'] == True:
                    self.line_ids = [(0,0, {
                        'date': r['holiday_date'],
                        'description': r['holiday_name'],
                        'type': 'holiday'
                    })]
        except:
            raise UserError(("Error Connection."))

class WorkingCalendarLine(models.Model):
    _name = 'working.calendar.line'
    _description = 'Working Calendar Line'
    
    working_calendar_id = fields.Many2one('working.calendar', string='Working Calendar')
    date = fields.Date('Date')
    description = fields.Char('Description')
    day = fields.Char('Day', compute="_get_days")
    type = fields.Selection([
        ('holiday', 'Public Holiday'),
        ('event', 'Internal Event'),
    ], string='Type')

    @api.depends('date')
    def _get_days(self):
        for rec in self:
            if rec.date:
                rec.day = rec.date.strftime("%A")
            else:
                rec.day = False